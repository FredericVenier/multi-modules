package net.tncy.venier.bookstore.data;

import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import java.util.Set;

import static org.junit.Assert.*;

public class BookTest {

    @Test
    public void test() {
        Book b = new Book("Intégrale Le Seigneur des Anneaux (Nouvelle traduction)", "J.R.R. tolkien", "2266286269");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(b);
        assertEquals(0, constraintViolations.size());
    }
}